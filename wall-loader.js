class WallLoader extends Application {
    constructor (app) {
        super(app);
        this.WallType = "w";
        this.DoorType = "d";
        this.curveSamples = 6;
        this.max_dist_percent = 10.;
        this.light_ratio = 50;
        this.scale = 1;
    }

    init() {
        this.activateHooks();
    }

    static get defaultOptions() {
        const options = super.defaultOptions;
        options.template = "modules/svg-loader/menu.html";
        options.width  = "600";
        options.height = "auto";
        options.resizable=true;
        return options;
    }

    getData() {
        return {
                sceneName: this.scene.data.name,
                sceneId: this.scene._id,
                maxDist: this.max_dist_percent,
                lightRatio: this.light_ratio,
                scale: this.scale
                };
    }

    activateListeners(html) {
        $(".startImport").click ( ev => {
            ev.preventDefault();

            let file = html.find("input[name=fileUploads]").prop('files');
            this.readFile(file).then( e => {
                console.time('Import Time');
                this.parseSVG(e);
                console.timeEnd('Import Time');
            });
        });

        // Set output for slider
        $(".DistSlider").on('input', (e) => {
            this.max_dist_percent = e.target.value;
            $(".DistOut")[0].innerHTML = e.target.value;
        });
        // Set output for slider
        $(".LightRatioSlider").on('input', (e) => {
            this.light_ratio = e.target.value;
            $(".LightRatioOut")[0].innerHTML = e.target.value;
        });

        $("input[name='scale']").on('input', ev => {
            this.scale = ev.target.value;
        });

        $(".OptionDetails").on('toggle', e => {
            if (e.target.open) {
                e.target.children[0].innerHTML = "Hide";
            } else {
                e.target.children[0].innerHTML = "Show";
            }
        });
        $(".DescDetails").on('toggle', e => {
            if (e.target.open) {
                e.target.children[0].innerHTML = "Hide description";
            } else {
                e.target.children[0].innerHTML = "Show description";
            }
        });
    }

    activateHooks() {
        Hooks.on('getSceneDirectoryEntryContext', (html, options) => {
            options.push({
								name: "Import SVG Data",
								icon: '<i class="fas fa-vector-square"></i>',
                callback: li => {
                    let scene = game.scenes.get(li[0].getAttribute("data-entity-id"));
                    scene.view();
                    this.scene = scene;
                    this.render(true);
                }
            });
        });
    }

    /**
     *
    **/
    async readFile(files) {
        for (let i = 0; i < files.length; i++) {
            const file = files[i];
            if (file) {
                return new Promise((resolve, reject) => {
                    const reader = new FileReader();
                    reader.onload = (e) => {

                        let tmp = document.createElement('div');
                        tmp.innerHTML = e.target.result;

                        let svg = tmp.getElementsByTagName("svg")[0];
                        resolve(svg);
                    };
                    reader.readAsText(file);
                });
            }
        }
    }

    parseSVG(svg) {
        this.close();

        this.lights = [];
        this.walls = [];
        if (svg.getAttribute("xmlns:dgnfog"))
            this.parseDungeonFogSVG(svg, this.scene);
        else
            this.parseRegularSVG(svg, this.scene);

        const padding = [
            canvas.dimensions.paddingX,
            canvas.dimensions.paddingY
        ]
        if (this.scale != 1)    // Scale wall positions
            for (let i = 0; i < this.walls.length; i++) {
                for (let j = 0; j < 4; j++) {
                    let tmp = this.walls[i].c[j];
                    this.walls[i].c[j] = (tmp -  padding[j%2]) * this.scale + padding[j%2]
                }
            }

            console.log(this.lights);

        // canvas.scene.update({walls: this.walls, lights: this.lights});
        canvas.scene.createManyEmbeddedEntities('AmbientLight', this.lights);
        canvas.scene.createManyEmbeddedEntities('Wall', this.walls);
        ui.notifications.info("SUCCESS!");
    }

    parseRegularSVG(svg, scene) {
        console.log("Parsing regular SVG..");
        let paths = svg.getElementsByTagName("path");
        let lines = svg.getElementsByTagName("line");
        let circles = svg.getElementsByTagName("circle");

        let doors = [];
        let walls = [];
        this.lights = [];
        for (let i = 0; i < lines.length; i++)
            doors.push(this.parseDoor(lines[i]));

        for (let i = 0; i < paths.length; i++)
            this.parsePath(paths[i], scene, walls, "", doors);

        // resolve segments
        this.walls = this.concatWalls(walls);
        const offset = this.walls.length + 1;
        for (let i = 0; i < doors.length; i++) {
            this.walls.push({id: i+offset, t:"d", c:[doors[i][0].x, doors[i][0].y, doors[i][1].x, doors[i][1].y], move: 1, sense:1, door:1, ds: 0});
        }

        for (let i = 0; i < circles.length; i++)
            this.addLight(circles[i], i+1);


    }

	/**
     * Dungeonfog SVG parser.
	 */
	parseDungeonFogSVG(svg, scene) {
        console.log("Parsing DungeonFog SVG..");
        const grid_dim = canvas.dimensions.size;
        // This seems to be a good tradeoff between good looking and amount of walls used
        this.max_dist = grid_dim * (this.max_dist_percent / 100.);

		let nodes = svg.children;

		// for each top level node
		for (let i = 0; i < nodes.length; i++) {
			let id = nodes[i].id;

			// room polygons
			if (id == "rooms") {
				this.parseRooms(nodes[i].children, scene);
			}
			// props
			if (id == "props") {
				this.parseProps(nodes[i].children, scene);
			}
		}

	}

	/**
	 * Parse through rooms
	 */
	parseRooms(roomList, scene) {
        let walls = []; // gather wall data
        let doors = []; // gather door data

		// for each room
		for (let i = 0; i < roomList.length; i++) {
			let roomId = roomList[i].id;
			let roomElements = roomList[i].children;
            // Get rooms and doors
            let paths = [];
            for (let j = 0; j < roomElements.length; j++) {
                const roomElem = roomElements[j];
                const tag = roomElem.tagName;
                switch(tag) {
					case "path":
                        paths.push(roomElem);
						break;
					case "line":
						// possible door
						const dgnfogType = roomElem.getAttribute("dgnfog:type");
                        if (dgnfogType === 'door') {

                            doors.push(this.parseDoor(roomElem));
                        } else {
                            console.log(">> OTHER: dgnfog:type = " + dgnfogType);
                        }
						break;
					default:
						console.log(">> OTHER: " + tag);
                }
            }

            for (let j = 0; j < paths.length; j++) {
                const dgnfogHidden = paths[j].getAttribute("dgnfog:hidden");
                this.parsePath(paths[j], scene, walls, dgnfogHidden, doors);
            }
        }



		// resolve segments
        this.walls = this.concatWalls(walls);

        const offset = this.walls.length + 1;
        for (let i = 0; i < doors.length; i++) {
            this.walls.push({id: i+offset, t:"d", door: CONST.WALL_DOOR_TYPES.DOOR, c:[doors[i][0].x, doors[i][0].y, doors[i][1].x, doors[i][1].y], s:0, });
        }
	}

    parseDoor(line) {
        const p1 = new Point2D(parseFloat(line.getAttribute('x1')), parseFloat(line.getAttribute('y1')));
        const p2 = new Point2D(parseFloat(line.getAttribute('x2')), parseFloat(line.getAttribute('y2')));
        return [p1,p2];
    }

	/**
	 * Parse through props, we're only interested in props that have light sources
	 */
	parseProps(propList, scene) {
            // for each prop
            let id = 1;
            for (let i = 0; i < propList.length; i++) {
                const propId = propList[i].id;
                const prop = propList[i].children;

                for (let j = 0; j < prop.length; j++) {
                    const tag = prop[j].tagName;
                    switch(tag) {
                        case "circle":
                            // parse lights
                            const dgnfogType = prop[j].getAttribute("dgnfog:type");
                            if (dgnfogType === "light")
                                this.addLight(prop[j], id++);
                            else
                                console.log(">> Not light, but: " + dgnfogType);
                            break;
                        case "rect":
                            // Possible future feature of importing props/shapes?
                            break;
                        default:
                            console.log(">> Other: " + tag);
                    }
                }
            }
	}

    addLight(light, id) {
        let MyRgbToHex = (rgb) => {
            return '#' + Number(rgb[0]).toString(16) + Number(rgb[1]).toString(16) + Number(rgb[2]).toString(16);
        }
        const fill = light.getAttribute('fill');
        const color = MyRgbToHex(fill.substring(4, fill.length - 1).split(','));
        
        let center = new Point2D(light.getAttribute("cx"), light.getAttribute("cy"), false);
        center = Point2D.mult(center, this.scale);
        center = new Point2D(center.x, center.y)
        const rad = light.getAttribute("r") * canvas.scene.data.gridDistance / canvas.scene.data.grid *this.scale;
        const dim = rad;
        const bright = rad * this.light_ratio / 100.;
        this.lights.push({x: center.x, y:center.y, bright:bright, dim: dim, t: "l", tintColor: color, tintAlpha: 0.5, darknessThreshold: 0, angle: 360});
    }

	/**
	 * Parse path modifed to include the negation of hidden segments in the path
	 */
    parsePath(path, scene, walls, hidden, doors) {
        const hiddenIdxs = hidden === "" ? new Set() : new Set(path.getAttribute("dgnfog:hidden").split(/\s/).map(e => {return parseInt(e);}));

        const color = path.getAttribute("stroke");
        let list = path.getAttribute("d").split(/(?=[LMCA])/);
        let commands = [];
        let pos = [];

		for (let b = 0; b < list.length; b++) {
			let el = list[b];
			commands.push(el.substr(0,1));
			pos.push(el.substr(1).split(/\s|,/g));
		}

		// for hidden walls.. we need to continue following the 'path' but exclude adding it to a wall array
        var newPos = new Point2D(0,0);
        var oldPos = new Point2D(0,0);
        for (let i = 0; i < commands.length; i++) {
            if (hiddenIdxs.has(i-1)) {  // indices of wall elements start one earlier, since "Move" command is ignored.
                newPos = new  Point2D(pos[i][pos[i].length-2], pos[i][pos[i].length-1]);
            } else {
                switch (commands[i]) {
                    case "M": newPos = new Point2D(pos[i][0], pos[i][1]); break;
                    case "L": newPos = new Point2D(pos[i][0], pos[i][1]);
                              this.evaluate_path(oldPos, newPos, walls, doors);
                              break;
                    case "C": newPos = new Point2D(pos[i][4], pos[i][5]);
                              this.evaluateCubic(oldPos, new Point2D(pos[i][0],pos[i][1]), new   Point2D(pos[i][2], pos[i][3]), newPos, walls, doors);
                              break;
                    case "A": newPos = new Point2D(pos[i][5], pos[i][6]);
                              this.evaluateEllipse(oldPos, pos[i], walls, doors);
                              break;
                }
            }
            oldPos = newPos;
        }
    }

    /**
     * Checks if a door is on the given path. Not working: Door bigger than a line segment. This may result in a wrong behaviour.
     * @return Array of new wall segments
    **/
    evaluate_path(p0, p1, walls, doors_orig) {
        var doors = JSON.parse(JSON.stringify(doors_orig));
        // Gather distance to p0, so we can evaluate multiple doors on the wall
        let door_dist_to_p0 = [];

        // Check for each door if they are intersecting with the current wall
        for (let i = 0; i < doors.length; i++) {
            if (this.concat_wall_segments([p0,p1], doors[i]).length == 1) {
                // Save in array
                door_dist_to_p0.push({
                                        dist:[
                                               Point2D.length2(Point2D.sub(p0, doors[i][0])),
                                               Point2D.length2(Point2D.sub(p0, doors[i][1]))
                                             ],
                                        idx: i
                                    });
            }
        }


        for (let i = 0; i < door_dist_to_p0.length; i++) {
            let idx = door_dist_to_p0[i].dist[0] < door_dist_to_p0[i].dist[1] ? 0 : 1;
            walls.push([p0, doors[door_dist_to_p0[i].idx][idx]]);
            p0 = doors[door_dist_to_p0[i].idx][(idx+1)%2];
        }
        walls.push([p0,p1]);
    }

    /**
     * Samples a cubic bezier spline in a similar method to the ellipse sampling.
     * Checks if center of current line is farther away than max_dist
     * if yes, it samples curve at that point, else returns current line.
    **/
    evaluateCubic(x0, x1, x2, x3, walls, doors_orig) {
        var doors = JSON.parse(JSON.stringify(doors_orig));
        let curve_points = [];
        let door_points = [];
        // Checks if doors are on the cubic bezier
        // saves them in door_points
        for (let i = 0; i < doors.length; i++) {
            let tmp = this.door_on_cubic(doors[i], x0,x1,x2,x3);
            tmp.doors_idx = i;
            if (tmp.length == 2)
                door_points.push(tmp);
        }
        let t0 = 0;
        let p0 = x0;
        let t1 = 1;
        let p1 = x3;
        if (door_points.length > 0) {
            for (let i = 0; i < door_points.length; i++) {
                const p = this.evaluate_cubic_point(x0,x1,x2,x3, door_points[i][0].t);
                doors_orig[door_points[i].doors_idx][0] = p;    // Update original door with new point for better alignment
                let tmp = this.evaluate_center_cubic(this.max_dist,
                                                        t0, p0,
                                                        door_points[i][0].t,
                                                        p, //doors[door_points[i].doors_idx][door_points[i][0].idx],
                                                    x0,x1,x2,x3);
                curve_points = curve_points.concat(tmp);
                t0 = door_points[i][1].t;
                p0 = this.evaluate_cubic_point(x0,x1,x2,x3, t0); //doors[door_points[i].doors_idx][door_points[i][1].idx];
                doors_orig[door_points[i].doors_idx][1] = p0; // Update original door with new point for better alignment
            }
            let tmp = this.evaluate_center_cubic(this.max_dist,
                                                    t0, p0,
                                                    t1, p1,
                                                x0,x1,x2,x3);
            curve_points = curve_points.concat(tmp);
        } else {
            curve_points = this.evaluate_center_cubic(this.max_dist, 0, x0, 1, x3, x0, x1, x2, x3);
        }
        for (var i = 0; i < curve_points.length; i++) {
            walls.push(curve_points[i]);
        }
    }

    /**
     * Checks if a door is on a cubic spline by iterating in small steps over the curve and checking
     * if it intersects with the door.
     * @returns array of tuples if found. Tuple: {idx:door_idx, t: time}
     *                  else empty array
    **/
    door_on_cubic(door, x0,x1,x2,x3) {
        const max_samples = 200;
        const eps = 10;
        const dt = 1. / (max_samples);
        let ret = []
        let idx0_found = false;
        let idx1_found = false;
        for (let i = 0; i < max_samples; i++) {
            const t = i*dt;
            const p = this.evaluate_cubic_point(x0,x1,x2,x3,t);
            const dist1 = Point2D.length2(Point2D.sub(door[0], p));
            const dist2 = Point2D.length2(Point2D.sub(door[1], p));
            if (dist1 < eps && !idx0_found) {
                ret.push({idx:0, t: t});
                idx0_found = true;
            }
            if (dist2 < eps && !idx1_found) {
                ret.push({idx:1, t: t});
                idx1_found = true;
            }
            if (idx0_found && idx1_found)
                break;
        }
        return ret;
    }

    evaluate_center_cubic(max_dist, t0, p0, t1, p1, x0, x1, x2, x3) {
        const t = (t0+t1) / 2.;
        const line_center = Point2D.mult(Point2D.add(p0, p1), (1./2.));
        const p_cubic = this.evaluate_cubic_point(x0,x1,x2,x3,t);
        const dist = Point2D.length(Point2D.sub(p_cubic, line_center));
        if (Math.abs(t0-t1) < 1e-3) {
            return [];
        } else if (dist <= max_dist) {
            return [[p0, p1]];
        } else {
            let first_half = this.evaluate_center_cubic(max_dist, t0,p0,t,p_cubic,x0,x1,x2,x3);
            let second_half = this.evaluate_center_cubic(max_dist, t,p_cubic,t1,p1,x0,x1,x2,x3);
            return first_half.concat(second_half);
        }
    }

    /**
     * Evaluate cubic bezier spline at position t
     * @param x0  control points
     * @param x1  control points
     * @param x2  control points
     * @param x3  control points
     * @return evaluated point
     */
    evaluate_cubic_point(x0, x1, x2, x3, t) {
        const x = x0.x * Math.pow(1.-t,3) + 3 * Math.pow(1. - t, 2) * t * x1.x + 3 * (1-t) * t * t * x2.x + t*t*t*x3.x;
        const y = x0.y * Math.pow(1.-t,3) + 3 * Math.pow(1. - t, 2) * t * x1.y + 3 * (1-t) * t * t * x2.y + t*t*t*x3.y;
        return new Point2D(x,y,false);
    }

    // Creates elliptical arc according to: https://beta.observablehq.com/@jrus/svg-elliptical-arcs
    evaluateEllipse(oldPos, params, walls, doors_orig) {
        var doors = JSON.parse(JSON.stringify(doors_orig));
        // Pre calculations, following the formulas from www.w3.org/TR/SVG/
        const RADIANS = Math.PI/180;
        var
            rx = Math.abs(params[0]),
            ry = Math.abs(params[1]);
        const
            phi = params[2] * RADIANS,
            large_arc_flag = params[3],
            sweep_flag = params[4],
            p1 = new Point2D(params[5], params[6], false),
            p0 = new Point2D(oldPos.x-canvas.dimensions.paddingX, oldPos.y-canvas.dimensions.paddingY, false);

        if (p0.x === p1.x && p0.y === p1.y)
            return;

        if (rx === 0 || ry ===0 )
        return;

        // Following "Conversion from endpoint to center parameterization"
		// http://www.w3.org/TR/SVG/implnote.html#ArcConversionEndpointToCenter

        // Step #1: Compute transformed Point2D
        const dx = (p0.x - p1.x)*0.5,
              dy = (p0.y - p1.y)*0.5;
        const p_transf = new Point2D (
                                     Math.cos(phi)*dx + Math.sin(phi)*dy,
                                    -Math.sin(phi)*dx + Math.cos(phi)*dy,
                                    false);

        const radiiCheck = p_transf.x * p_transf.x / (rx*rx) + p_transf.y*p_transf.y / (ry*ry);
        if (radiiCheck > 1) {
            rx = Math.sqrt(radiiCheck)*rx;
            ry = Math.sqrt(radiiCheck)*ry;
        }

        // Step #2: Compute transformed Center
        const cCoeff_Numerator = (rx * rx * ry * ry - rx * rx * p_transf.y * p_transf.y - ry * ry * p_transf.x * p_transf.x);
        const cCoeff_Denom     = ( rx*rx*p_transf.y*p_transf.y + ry*ry*p_transf.x*p_transf.x);
        var cCoeff = (rx * rx * ry * ry - rx * rx * p_transf.y * p_transf.y - ry * ry * p_transf.x * p_transf.x)
                     / ( rx*rx*p_transf.y*p_transf.y + ry*ry*p_transf.x*p_transf.x);
        cCoeff = cCoeff < 0 ? 0 : cCoeff; // Make sure precision doesn't make it drop below 0
        cCoeff = (large_arc_flag != sweep_flag ? 1 : -1) * Math.sqrt(cCoeff);
        const c_transf = new Point2D(
                                    cCoeff*((rx*p_transf.y)/ry),
                                    cCoeff*(-(ry*p_transf.x)/rx),
                                    false);

        // Step #3: compute Center
        const c = new Point2D(
                            Math.cos(phi)*c_transf.x - Math.sin(phi)*c_transf.y + ((p0.x+p1.x)/2),
                            Math.sin(phi)*c_transf.x + Math.cos(phi)*c_transf.y + ((p0.y+p1.y)/2),
                            false);
        // Step #4: start and sweep angles


        const v0 = new Point2D(((p_transf.x - c_transf.x) / rx), ((p_transf.y - c_transf.y) / ry), false);
        const x_axis = new Point2D(1, 0, false);
        const theta_0 = this.calcAngle(x_axis, v0);
        const v1 = new Point2D(((-p_transf.x - c_transf.x) / rx), ((-p_transf.y - c_transf.y) / ry), false);
        const d_theta = this.calcAngle(v0, v1);

        const offset = new Point2D(0., 0., true);
        let door_points = [];
        let curve_points = [];

        for (let i = 0; i < doors.length; i++) {
            let tmp = this.door_on_ellipsis(doors[i], theta_0, d_theta, phi, rx, ry, c, offset);
            tmp.doors_idx = i;
            if (tmp.length == 2)
                door_points.push(tmp);
        }

        let t0 = 0;
        let t1 = 1;
        let p_start = p0;

        if (door_points.length > 0) {
            for (let i = 0; i < door_points.length; i++) {
                const alpha = theta_0 + door_points[i][0].t*d_theta;
                //evaluate_ellipse_point(t, alpha, phi, rx, ry, c) {
                const p = this.evaluate_ellipse_point(door_points[i][0].t, alpha, phi, rx, ry, c);
                doors_orig[door_points[i].doors_idx][0] = Point2D.add(p, offset);    // Update original door with new point for better alignment
                let tmp = this.evaluate_center(this.max_dist, t0, p_start, door_points[i][0].t, p, theta_0, d_theta, phi, rx, ry, c);
                curve_points = curve_points.concat(tmp);
                const alpha1 = theta_0 + door_points[i][1].t*d_theta;
                t0 = door_points[i][1].t;
                p_start = this.evaluate_ellipse_point(door_points[i][1].t, alpha1, phi, rx, ry, c);
                doors_orig[door_points[i].doors_idx][1] = Point2D.add(p_start, offset);    // Update original door with new point for better alignment
            }
            let tmp = this.evaluate_center(this.max_dist, t0, p_start, t1, p1, theta_0, d_theta, phi, rx, ry, c);
            curve_points = curve_points.concat(tmp);
        } else {
            curve_points = this.evaluate_center(this.max_dist, 0, p0, 1, p1, theta_0, d_theta, phi, rx, ry, c);
        }
        // Add canvas padding to curve points
        for (let i = 0; i < curve_points.length; i++) {
            curve_points[i][0] = Point2D.add(curve_points[i][0], offset);
            curve_points[i][1] = Point2D.add(curve_points[i][1], offset);
            walls.push(curve_points[i]);
        }
    }

    /**
     * Checks if a door is on ab ellipsis curve by iterating in small steps over the curve and checking
     * if it intersects with the door.
     * @returns array of tuples if found. Tuple: {idx:door_idx, t: time}
     *                  else empty array
    **/
    door_on_ellipsis(door, theta_0, d_theta, phi, rx, ry, c, offset) {
        const max_samples = Math.ceil(rx*ry); // Tested with 200, didn't work with big circles.
                                              // Like this it seems to work for all my test samples. Need more tests though!
        const eps = 1;
        const dt = 1. / (max_samples);
        let ret = []
        let idx0_found = false;
        let idx1_found = false;

        door[0] = Point2D.sub(door[0], offset);
        door[1] = Point2D.sub(door[1], offset);

        for (let i = 0; i < max_samples; i++) {
            const t = i*dt;
            const alpha = theta_0 + t*d_theta;
            const p = this.evaluate_ellipse_point(t, alpha, phi, rx, ry, c);
            const dist1 = Point2D.length(Point2D.sub(door[0], p));
            const dist2 = Point2D.length(Point2D.sub(door[1], p));
            if (dist1 < eps && !idx0_found) {
                ret.push({idx:0, t: t});
                idx0_found = true;
            }
            if (dist2 < eps && !idx1_found) {
                ret.push({idx:1, t: t});
                idx1_found = true;
            }
            if (idx0_found && idx1_found)
                break;
        }
        return ret;
    }

    calcAngle(v0, v1) {
        const sign = (v0.x*v1.y - v0.y*v1.x) < 0 ? -1 : 1;
        const len = (Point2D.length(v0) * Point2D.length(v1));
        if (len == 0)
            return 0;
        else
            return (sign * Math.acos(Point2D.dot(v0,v1) / len));
    }

    /**
     * Evaluate point at pos t of given ellipsis
     * From http://www.w3.org/TR/SVG/implnote.html#ArcParameterizationAlternatives
    **/
    evaluate_ellipse_point(t, alpha, phi, rx, ry, c) {
        const ex = rx * Math.cos(alpha),
              ey = ry * Math.sin(alpha);

         return new Point2D(
                              Math.cos(phi)*ex - Math.sin(phi)*ey + c.x,
                              Math.sin(phi)*ex + Math.cos(phi)*ey + c.y,
                              false);
    }

    /**
        Idea: Sample once in the center of the arc.
        Check at first and third quarter if to far away (dist < max dist)
        if not: finish
        else: add sample point at point.
        Repeat
    **/
    evaluate_center(max_dist, t_start, p_start, t_end, p_end, theta_0, d_theta, phi, rx, ry, c) {
        const t = (t_start + t_end) / 2.;
        const alpha = theta_0 + t*d_theta;
        const p_ellipsis  = this.evaluate_ellipse_point(t, alpha, phi, rx, ry, c);
        const path_center = Point2D.mult(Point2D.add(p_start, p_end), (1./2.));
        const dist = Point2D.length(Point2D.sub(p_ellipsis, path_center));
        if (Math.abs(t_start-t_end) < 1e-3) {
            return [];
        } else if (dist <= max_dist) {
            return [[p_start, p_end]];
        } else {
            let first_half = this.evaluate_center(max_dist, t_start, p_start, t, p_ellipsis, theta_0, d_theta, phi, rx, ry, c);
            let second_half = this.evaluate_center(max_dist, t, p_ellipsis, t_end, p_end, theta_0, d_theta, phi, rx, ry, c);
            return first_half.concat(second_half);
        }
    }

    /**
     * Checks if point p is colinear to wallsegment w.
    **/
    orientation(p,w) {
        // See https://www.geeksforgeeks.org/orientation-3-ordered-points/ for formula
        const val = (w[0].y-p.y)*(w[1].x-w[0].x)-(w[0].x-p.x)*(w[1].y-w[0].y);
        return (val == 0);
    }

    /**
     * Given a point q colinear to wall segment w, checks if point q lies on wall segment w.
    **/
    onSegment(q,w) {
        return ((q.x <= Math.max(w[0].x,w[1].x) && q.x >= Math.min(w[0].x,w[1].x))
                && (q.y <= Math.max(w[0].y,w[1].y) && q.y >= Math.min(w[0].y,w[1].y)));
    }

    /**
     * Checks if two lines are intersecting. and concatenates them if possible
     * @param w0 tuple of endpoints from wall 0
     * @param w1 tuple of endpoints from wall 1
     * @return Array of new wall segments.
    **/
    concat_wall_segments(w0, w1) {
        // Check for poth points of w1 if they are colinear to w0.
        // If not return wall the wall segments.
        if (!this.orientation(w1[0], w0)
            || !this.orientation(w1[1], w0))
            return [w0, w1];



        // Check for intersection
        let w0_in_w1 = [];
        w0_in_w1.push(this.onSegment(w0[0],w1));
        w0_in_w1.push(this.onSegment(w0[1],w1));
        let w1_in_w0 = [];
        w1_in_w0.push(this.onSegment(w1[0],w0));
        w1_in_w0.push(this.onSegment(w1[1],w0));

        // Evaluate intersections

        // Case 0:
        // w0 is completly in w1
        if (w0_in_w1[0] && w0_in_w1[1]) {
            // Remove w0, return only w1
            return [w1];
        }
        // Case 1:
        // w1 is completly in w0
        if (w1_in_w0[0] && w1_in_w0[1]) {
            // Remove w1 and return only w0
            return [w0];
        }
        // Case 2 - 6:
        // Both walls intersecting on one side.
        for (let i = 0; i < 2; i++) {
            for (let j = 0; j < 2; j++) {
                if (w0_in_w1[i] && w1_in_w0[j]) {
                    // Return a wall consisting of the other segment respectively
                    return [[w0[(i+1)%2], w1[(j+1)%2]]];
                }
            }
        }

        return [w0,w1];
    }

    concatWalls(walls) {
        let newWalls = [];
        const eps = 1e-3;
        var dif = true;
        // Check as long as there are still fewer walls remaining after concatenation.
        // This is safe to end pretty soon. Also there may be a smarter way to to this but.. pff The Perforamnce difference woulud be negligible
        while(dif) {
            newWalls = [];
            // Set of wall segments already compared
            let idx_added_set = new Set();
            // For each wall segment
            for (var i = 0; i < walls.length; i++) {
                const w0 = walls[i];
                for (var j = i + 1; j < walls.length && !idx_added_set.has(i); j++) {
                    if (idx_added_set.has(j))
                        continue;
                    const w1 = walls[j];
                    // Concat wall segments if possible
                    const tmp = this.concat_wall_segments(w0,w1);
                    // If they were concated, add segment to new wall array and safe indices in set
                    if (tmp.length == 1) {
                        newWalls.push(tmp[0]);
                        idx_added_set.add(i);
                        idx_added_set.add(j);
                    }
                }
                // Seems like this wall segment has no concat partner.
                if (!idx_added_set.has(i)) {
                    idx_added_set.add(i);
                    newWalls.push(w0);
                }
            }

            dif = (newWalls.length < walls.length);
            walls = newWalls;
        }
        walls = [];
        for (var i = 0; i < newWalls.length; i++) {
            walls.push({id: i+1, t:"w", c:[newWalls[i][0].x, newWalls[i][0].y, newWalls[i][1].x, newWalls[i][1].y], s:0});
        }
        return walls;
    }
}


class Point2D{
    constructor(x,y, padding=true) {
        this.x = (typeof(x) === "string") ? parseFloat(x) : x;
        this.y = (typeof(y) === "string") ? parseFloat(y) : y;
        if (padding) {
            this.x += canvas.dimensions.paddingX;
            this.y += canvas.dimensions.paddingY;
        }
    }
    length() {
        return Point2D.length(this);
    }
    // dot product
    static dot(a, b) {
        return a.x * b.x + a.y * b.y;
    }
    // length
    static length(a) {
        return Math.sqrt(a.x*a.x + a.y*a.y);
    }
    // length squared
    static length2(a) {
        return (a.x*a.x + a.y*a.y);
    }
    // vector subtraction
    static sub(a,b) {
        return new Point2D(a.x-b.x, a.y-b.y, false);
    }
    // vector addition
    static add(a,b) {
        return new Point2D(a.x+b.x, a.y+b.y, false);
    }
    // Scalar multiplication
    // a : vector
    // b : scalar
    static mult(a,b) {
        return new Point2D(b*a.x, b*a.y, false);
    }
};

let svgloader = new WallLoader();
svgloader.init();
